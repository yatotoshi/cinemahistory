import datetime
from django.utils import timezone
from django.db import models

class Area(models.Model):
    area_name = models.CharField('name of area', max_length=100)
    area_name_ru = models.TextField('name of area (rus)', max_length=100)

    def __str__(self):
        return self.area_name

    #class Meta:
    #    verbouse_name = 'Часть света'
    #    verbouse_name_plural = 'Части света'

class Article(models.Model):
    article_title = models.CharField('article title', max_length=200)
    article_text = models.TextField('article body')
    pub_date = models.DateTimeField('publishing date')
    area = models.ForeignKey(Area, on_delete=models.CASCADE)

    def __str__(self):
        return self.article_title

    def was_published_recently(self):
        return self.pub_date >= (timezone.now() - datetime.timedelta(days=7))

    #class Meta:
    #    verbouse_name = 'Статья'
    #    verbouse_name_plural = 'Статьи'

class Comment(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    author_name = models.CharField('author name', max_length=50)
    comment_text = models.CharField('comment text', max_length=200)

    def __str__(self):
        return self.author_name

    #class Meta:
    #    verbouse_name = 'Комментарий'
    #    verbouse_name_plural = 'Комментарии'