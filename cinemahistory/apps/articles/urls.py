from django.urls import path
from . import views

app_name = 'articles'
urlpatterns = [
    path('', views.index, name = 'index'),
    path('area/<area_name>/', views.area, name='area'),
    path('article/<int:article_id>/', views.detail, name='detail'),
    path('?search=<search_query>/', views.search, name='search'),
]