from django.contrib import admin
from .models import Area, Article, Comment

admin.site.register(Area)
admin.site.register(Article)
admin.site.register(Comment)