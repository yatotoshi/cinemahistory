from django import template
from articles.models import Area, Article

register = template.Library()

@register.simple_tag
def get_areas(context):
    areas = Area.objects.all()[:3]
    return {'areas': areas}

@register.simple_tag
def get8articles(context):
    last_articles = Article.objects.order_by("-pub_date")[:8]
    return {'last_articles': last_articles}

@register.simple_tag
def get_other(context):
    try:
        other_articles = Area.objects.get(area_name="Other").article_set.all()
        return {'other_articles': other_articles}
    except:
        print('DB is seems to be empty')
        return {'other_articles': [Article(article_title='none', id=0)]}
