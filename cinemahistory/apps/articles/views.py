from django.shortcuts import render
from .models import Area, Article, Comment
from django.http import Http404, HttpResponseRedirect
from django.urls import reverse


def index(request):
    #articles_list = Article.objects.all()
    #return render(request, 'articles/list.html', {'articles_list': articles_list})
    if 'search' in request.GET:
        return search(request)
    return render(request, 'pages/index.html')

def detail(request, article_id):
    if 'search' in request.GET:
        return search(request)
    try:
        a = Article.objects.get(id = article_id)
    except:
        raise Http404("Статья не найдена!")
    return render(request, 'pages/detail.html', {'article': a})

def area(request, area_name):
    if 'search' in request.GET:
        return search(request)
    try:
        a = Area.objects.get(area_name = area_name)
    except:
        raise Http404("Статья не найдена!")
    articles_list = a.article_set.all()

    return render(request, 'pages/area.html', {'area': a, 'articles_list': articles_list})

def search(request):
    a = Article.objects.filter(article_title__contains=request.GET['search'])
    return render(request, 'pages/search_results.html', {'articles_list': a, 'query': request.GET['search']})